# Waybar config

![Size](https://img.shields.io/github/repo-size/appuchias/waybar?color=orange&style=flat-square)
[![Author](https://img.shields.io/badge/Project%20by-Appu-9cf?style=flat-square)](https://gitlab.com/appuchia)

My Waybar config.

Still in development.

## License

This code is licensed under the [GPLv3 license](https://gitlab.com/appuchia-dotfiles/waybar/-/blob/master/LICENSE).

Coded with 🖤 by Appu
